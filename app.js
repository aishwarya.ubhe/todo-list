var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/todolist');

app.set("view engine","ejs");
app.set('views','./views');
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));

var todoSchema = new mongoose.Schema({
  name: String
});

var Todo = mongoose.model('Todo',todoSchema);

app.get('/',function(req,res){
  Todo.find({}, function(err, todoList){
    if (err) {
      console.log(err);
    }else{
      res.render("index.ejs", {todoList: todoList});
    }
  });
});

app.post('/newtodo',function(req,res){
  console.log("List submitted");
  var newItem = new Todo({
    name: req.body.item
  });
  Todo.create(newItem, function(err, Todo){
    if(err) console.log(err);
    else{
      console.log("Inserted items " +newItem);
    }
  })
  res.redirect("/");
});

app.get('*',function(req,res){
  res.send("<h1>Invalid routes</h1>");
});

app.listen(3000,function(){
  console.log("Server listening on 3000");
});